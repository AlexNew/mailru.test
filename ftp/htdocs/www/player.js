/* @preserve Created by Alex Novikov www.asnovikov.ru on 26/11/14. */
/*global angular, Audio, controller */
/*jshint bitwise:true, camelcase:true, curly:true, eqeqeq:true, es3:true, forin:true, freeze:true, immed:true, newcap:true*/
/*jshint noarg:true, noempty:true, nonew:true, plusplus:true, undef:true, strict:true, trailing:true, browser:true */

(function () {
    'use strict';
    angular.module('MusicPlayer', [])

        .controller('PlayerController', ['$scope', '$timeout', function ($scope, $timeout) {
            //https://developer.mozilla.org/en-US/docs/Web/API/HTMLMediaElement
            var audio = new Audio('Beethoven-Moonlight_Sonata.mp3');
            // The Model
            $scope.paused = audio.paused;
            $scope.muted = audio.muted;
            $scope.duration = audio.duration;
            $scope.currentTime = audio.currentTime;
            $scope.volume = audio.volume;

            // play/pause button
            $scope.playpause = function () {
                //audio.paused ? audio.play() : audio.pause();  //shorter, but suspiciously: can be cut off by the compiler
                if (audio.paused) {
                    audio.play();
                } else {
                    audio.pause();
                }
                $scope.paused = audio.paused;
            };

            // mute/unmute button
            $scope.muteunmute = function () {
                $scope.muted = audio.muted = !audio.muted;
            };

            // user changed current position
            $scope.changePosition = function () {
                audio.currentTime = this.currentTime;
            };

            // user changed volume
            $scope.changeVolume = function () {
                audio.volume = this.volume;
            };

            // get track duration before play
            audio.addEventListener('loadedmetadata', function () {
                var that = this;
                $timeout(function () {
                    $scope.duration = that.duration;
                });
            });

            // update current position - move the progressBar
            audio.addEventListener('timeupdate', function () {
                var that = this;
                $timeout(function () {
                    $scope.currentTime = that.currentTime;
                });
            });

            // track has finished -> moving to the start
            audio.addEventListener('ended', function () {
                $timeout(function () {
                    $scope.paused = true;
                    $scope.currentTime = 0;
                });
            });

        }]);

}());