/* @preserve Created by Alex Novikov www.asnovikov.ru on 25.11.14. */
/*global QUnit, expect, console, setTimeout */
/*jshint bitwise:true, camelcase:true, curly:true, eqeqeq:true, es3:true, forin:true, freeze:true, immed:true, newcap:true*/
/*jshint noarg:true, noempty:true, nonew:true, plusplus:true, undef:true, strict:true, trailing:true, browser:true */

(function () {
    'use strict';

    function asyncForeach(arr, callBack, finish) {
        var i = 0,
            iterate = function () {
                i += 1;
                if (i < arr.length) {
                    callBack(arr[i], i, iterate);
                } else {
                    finish();
                }
            };
        callBack(arr[i], i, iterate);
    }

    /**
     * Testing Drive Development of asyncForeach function
     *
     * Test cases:
     * +asyncForeach call function in second argument (callback func)
     * +asyncForeach call function in third argument (finish func)
     * +callback have called for every array element
     * +callback have called with three parameters
     * +first argument of callback is array item
     * +second argument of callback is array index
     * +third argument of callback is function
     * +without calling third argument of callback asyncForeach should give only first element
     * +calling third argument of callback is moving to the next element of array
     * +finish func has called at the end
     **/

    QUnit.module('asyncForeach: Test-Driving Development');

    QUnit.asyncTest('Phase 1: callback & finish functions are calling', function (assert) {
        expect(2);

        asyncForeach([1], function (item, index, done) {
            assert.ok(true, 'asyncForeach call function in second argument (callback func)');
            done();
        }, function () {
            assert.ok(true, 'asyncForeach call function in third argument (finish func)');
            QUnit.start();
        });
    });

    QUnit.asyncTest('Phase 2: number of callback calls is equal to array length', function (assert) {
        var arr = [1, 2, 3, 4, 5];
        expect(arr.length);
        asyncForeach(arr, function (item, index, done) {
            assert.ok(true, 'callback have called for every array element');
            done();
        }, function () {
            QUnit.start();
        });
    });

    QUnit.asyncTest('Phase 3: number & type of callback parameters are correct', function (assert) {
        var arr = [77];
        asyncForeach(arr, function () {
            assert.strictEqual(arguments.length, 3, 'callback have called with three parameters');
            assert.strictEqual(arguments[0], arr[0], 'first argument of callback is array item');
            assert.strictEqual(arguments[1], 0, 'second argument of callback is array index');
            assert.strictEqual(typeof arguments[2], 'function', 'third argument of callback is function');
            QUnit.start();
        }, function () {});
    });

    QUnit.asyncTest('Phase 4: without calling third argument of callback asyncForeach should give only first element', function (assert) {
        var arr = [1, 2, 3, 4];
        expect(1);
        asyncForeach(arr, function () {
            assert.strictEqual(arguments[0], arr[0], 'first argument of callback is array item');
        }, function () {});
        setTimeout(function () {
            QUnit.start();
        }, 1000);
    });

    QUnit.asyncTest('Phase 5: calling third argument of callback is moving to the next element of array', function (assert) {
        var i = 0, arr = [1, 2, 3, 4];
        expect(arr.length * 2 + 1);
        asyncForeach(arr, function () {
            assert.strictEqual(arguments[0], arr[i], 'first argument of callback is array item');
            assert.strictEqual(arguments[1], i, 'second argument of callback is array index');
            i += 1;
            arguments[2]();
        }, function () {
            assert.strictEqual(i, arr.length, 'finish function has called at the end');
            QUnit.start();
        });
    });


    asyncForeach(
        [1, 2, 3],
        function (item, index, done) {
            setTimeout(function () {
                console.log(item);
                done();
            }, 1000);
        },
        function () {
            console.log('end');
        }
    );

}());